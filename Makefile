PROJECT = ska-eda-perf-eval
TANGO_HOST ?= tango-databaseds:10000## TANGO_HOST connection to the Tango DS
TANGO_SERVER_PORT ?= 45450## TANGO_SERVER_PORT - fixed listening port for local server

include .make/base.mk
include .make/helm.mk
include .make/k8s.mk
include .make/oci.mk
include .make/python.mk

DOCS_SPHINXOPTS = -n -W --keep-going

docs-pre-build:
	poetry config virtualenvs.create false
	poetry install --no-root --only docs

.PHONY: docs-pre-build

PYTHON_LINE_LENGTH = 88

VALUES_FILE ?= charts/eda.yaml
ifneq ($(VALUES_FILE),)
    CUSTOM_VALUES := --values $(VALUES_FILE)
else

endif

K8S_CHART_PARAMS = --set ska-eda-perf-eval.image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA) \
		--set global.tango_host=$(TANGO_HOST) \
		--set global.device_server_port=$(TANGO_SERVER_PORT) \
		--set ska-eda-perf-eval.image.registry=registry.gitlab.com/ska-telescope/ska-eda-perf-eval \
		${CUSTOM_VALUES}
