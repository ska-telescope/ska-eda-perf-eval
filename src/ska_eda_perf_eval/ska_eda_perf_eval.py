# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Connector project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
# pylint: disable=attribute-defined-outside-init

"""The PerfEval Tango Device provides a performance evaluation service."""

__copyright__ = "Copyright (c) 2023, CSIRO"
__author__ = "Guillaume Jourjon"
__email__ = "guillaume.jourjon@data61.csiro.au"
__version__ = "0.0.1"

import logging
import os
import random
import string
import subprocess
from datetime import datetime
from threading import Thread
from time import sleep

from ska_tango_base import SKABaseDevice
from ska_tango_base.commands import ResultCode  # FastCommand
from tango import AttrWriteType
from tango.server import attribute, command, run

from ska_eda_perf_eval.perf_component_manager import PerfComponentManager

# dictionary of types for attributes
ATTR_TYPES = {
    "int": int,
    "float": float,
    "str": str,
    "bool": bool,
}


class PerfEval(SKABaseDevice):
    """The PerfEval Tango Device provides a performance evaluation service."""

    # pylint: disable=too-many-instance-attributes

    # -----------------
    # Device Properties
    # -----------------

    # -----------------
    # Attributes
    # -----------------
    @attribute(dtype=int)
    def report_size(self):
        """
        Get Size of report.

        :return: integer
        """
        return self._size_of_report

    @attribute(dtype=float)
    def report_rate(self):
        """
        Get Report rate.

        :return: float number
        """
        return self._report_rate

    # ---------------
    # General methods
    # ---------------

    def create_component_manager(self):
        """Create the component manager."""
        self.logger.info("run create_component_manager")
        return PerfComponentManager(
            logger=self.logger,
            communication_state_callback=self._communication_state_changed,
            component_state_callback=self._component_state_changed,
        )

    def always_executed_hook(self):
        """Execute hook before any TANGO command is executed."""
        # PROTECTED REGION ID(LowCbfConnector.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  LowCbfConnector.always_executed_hook

    def delete_device(self):
        """Delete device allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(LowCbfConnector.delete_device) ENABLED START #
        # PROTECTED REGION END #    //  LowCbfConnector.delete_device

    def _get_report(self, attr):
        """Read a dynamic report attribute."""
        name = attr.get_name()
        if self._dtype_of_report == str:
            return self.__report_string(name.split("_")[-1])
        if self._dtype_of_report == int:
            return self.__report_int(name.split("_")[-1])
        if self._dtype_of_report == float:
            return self.__report_float(name.split("_")[-1])
        return None

    def _set_report(self, attr):
        """Ignore attribute writes."""

    def __report_string(self, number):
        """Create a report string for a given attribute number."""
        return f"{self._report} {number}"

    def __report_int(self, number):
        """Create a report integer for a given attribute number."""
        return int(self._report_int) + int(number)

    def __report_float(self, number):
        """Create a report float for a given attribute number."""
        return float(self._report_float) + float(number)

    # -----------------
    # Commands
    # -----------------

    @command(dtype_in=int)
    def create_report_attrs(self, n_reports: int):
        """Create ``n`` report attributes."""
        self._n_reports = n_reports
        for i in range(n_reports):
            if self._dtype_of_report == str:
                name = f"report_str_{i}"
            elif self._dtype_of_report == int:
                name = f"report_int_{i}"
            elif self._dtype_of_report == float:
                name = f"report_float_{i}"
            else:
                name = f"report_none_{i}"
            att = attribute(
                name=name,
                dtype=self._dtype_of_report,
                fget=self._get_report,
                fset=self._set_report,
                access=AttrWriteType.READ,
            )
            self.add_attribute(att)
            self.set_change_event(name, True, False)
            self.set_archive_event(name, True, False)

    def measure(self):
        """Start the performance evaluation."""
        while self.measurement_thread_daemon:
            if self._dtype_of_report == str:
                date = datetime.utcnow().strftime("%F %T.%f")[:-3]
                random_string = "".join(
                    random.choices(
                        string.ascii_lowercase, k=(self._size_of_report - len(date))
                    )
                )
                self._report = f"{random_string}:{date}"
                for i in range(self._n_reports):
                    self.push_change_event(f"report_str_{i}", self.__report_string(i))
                    self.push_archive_event(f"report_str_{i}", self.__report_string(i))
            elif self._dtype_of_report == int:
                self._report_int = random.randint(0, 4_294_967_295)
                for i in range(self._n_reports):
                    self.push_change_event(f"report_int_{i}", self.__report_int(i))
                    self.push_archive_event(f"report_int_{i}", self.__report_int(i))
            elif self._dtype_of_report == float:
                self._report_float = random.uniform(0.0, 32.0)
                for i in range(self._n_reports):
                    self.push_change_event(f"report_float_{i}", self.__report_float(i))
                    self.push_archive_event(f"report_float_{i}", self.__report_float(i))
            sleep(1.0 / self._report_rate)

    def __init__(self, *args, **kwargs):
        """Initialise the command handlers for commands supported by this device."""
        super().__init__(*args, **kwargs)

        self._size_of_report = 100
        self._report_rate = 10

        self.logger.warning("Init complete.")

    def init_command_objects(self):
        """Initialise the command handlers for commands supported by this device."""
        super().init_command_objects()
        self.logger.info("init_command_objects completed!")

    class InitCommand(SKABaseDevice.InitCommand):
        """Extend the SKABaseDevice class used for device initialisation."""

        def do(self, *args, **kwargs) -> tuple[ResultCode, str]:
            """Initialise the attributes and properties this device subclass."""
            # pylint: disable=protected-access
            super().do(*args, **kwargs)
            self._device.logger = logging.getLogger()
            self._device._size_of_report = 160
            self._device._report_rate = 1.0
            self._device._report = ""
            self._device._report_int = 1
            self._device._report_float = 0.0
            self._device._n_reports = 0
            self._device._dtype_of_report = int
            message = "Initialisation complete"
            self._device.logger.info(message)
            return ResultCode.OK, message

    @command()
    def start_report_thread(self):
        """Start the reporting thread."""
        self.measurement_thread = Thread(target=self.measure)
        self.measurement_thread_daemon = True
        self.measurement_thread.start()
        message = "Started reporting thread"
        self.logger.info(message)
        return ResultCode.OK, message

    @command()
    def stop_report_thread(self):
        """Stop the reporting thread."""
        self.measurement_thread_daemon = False
        message = "Stopped reporting thread"
        self.logger.info(message)
        return ResultCode.OK, message

    @command(
        dtype_in="DevShort",
        doc_in="Set size of report.",
    )
    def set_size_report(self, argin):
        """Set the size of the report."""
        self._size_of_report = argin
        message = f"Set report size to {self._size_of_report}"
        self.logger.info(message)
        return ResultCode.OK, message

    @command(
        dtype_in="DevFloat",
        doc_in="Set report rate.",
    )
    def set_report_rate(self, argin):
        """Set the report rate."""
        self._report_rate = argin
        message = f"Set report rate to {self._report_rate}"
        self.logger.info(message)
        return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="Set report type.",
    )
    def set_report_type(self, argin):
        """Set the report type."""
        self._dtype_of_report = ATTR_TYPES[argin]
        message = f"Set report type to {self._dtype_of_report}"
        self.logger.info(message)
        return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="File to write.",
    )
    def call_tcpdump(self, argin):
        """Set the report."""
        # pylint: disable=W1509,R1732,W0201
        tcpdump_call = ["tcpdump", "-i", "any", "-w", argin, "-s", "128"]
        self.pro = subprocess.Popen(
            tcpdump_call, stdout=subprocess.PIPE, preexec_fn=os.setsid
        )

        message = "Call tcpdump"
        self.logger.info(message)
        return ResultCode.OK, message

    @command()
    def kill_tcpdump(self):
        """Set the report."""
        self.pro.kill()
        message = "Kill tcpdump"
        self.logger.info(message)
        return ResultCode.OK, message


def main(args=None, **kwargs):
    """Start main function of the PerfEval module."""
    return run((PerfEval,), args=args, **kwargs)


if __name__ == "__main__":
    main()
