# ska-eda-perf-eval

Innovation sprint project to evaluate EDA performances.

In this project we have thus developed a simple Tango Device in order to test the SKA archiver
[EDA](https://gitlab.com/ska-telescope/ska-tango-archiver) that is heavily based on the canonical Tango
archiver [HDB++](https://gitlab.com/tango-controls/hdbpp).

Most of the test are done throught the notebooks from the `resources` folder.


## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-eda-perf-eval/badge/?version=latest)](https://developer.skao.int/projects/ska-eda-perf-eval/en/latest/?badge=latest)

The documentation for this project, including how to get started with it, can be found in the `docs` folder, or browsed in the SKA development portal:

* [ska-eda-perf-eval documentation](https://developer.skatelescope.org/projects/ska-eda-perf-eval/en/latest/index.html "SKA Developer Portal: ska-eda-perf-eval documentation")

## Features

* Configuration of report rate
* Configuration of the size of report for the string case
* Different type of reporting type: String, Integer, and Float
* Mechanism for capturing network traffic via tcpdump
* Notebooks to run the tests

## ToDo

* Transform notebooks to CI/CD test similar to what is done in [LOW CBD Integration Repo](https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-integration)
